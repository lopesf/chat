import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;



public class clientThread implements Runnable{

    private 	Socket clientSocket = null;
    private		ChatServer server = null;

    private		ArrayList<Room> rooms = null;
    private		ArrayList<String> joinIds = null;
	private 	ExecutorService threadPool;


    private 	BufferedReader 	is;
    private		PrintStream		os;


    public clientThread(Socket clientSocket, ChatServer server, ArrayList<Room> rooms, ArrayList<String> joinIds, ExecutorService threadPool) {
        this.clientSocket = clientSocket;
        this.server = server;

        this.rooms = rooms;
        this.joinIds = joinIds;
		this.threadPool = threadPool;

    }

	public void run() {
    	ArrayList<Room> rooms = this.rooms;
    	ArrayList<String> joinIds = this.joinIds;
    	ChatServer server = this.server;
		ExecutorService threadPool = this.threadPool;

        try {
        	
			is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			os = new PrintStream(clientSocket.getOutputStream());
			String	request;
			String[] requestSplit = {"", ""};

			while(true) {

				// test of server still active
				boolean stopped = false;
				synchronized(this) {
					stopped = (threadPool.isShutdown() || server.isStopped);
				}
				if(stopped) break;

				request = is.readLine();	// read request 1st line
				// System.out.println(this.toString()+ ": "+request);

				if(request == null) continue;	// test platform threads send null after kill service command given

				requestSplit = request.split(":");

				if(request.startsWith("HELO")) {
				
					InetAddress i = this.clientSocket.getLocalAddress();
					os.println(request + "\nIP:"+ i.getHostAddress().toString() + 
							"\nPort:" + this.clientSocket.getLocalPort() + "\nStudentID:11705259");
				}
				else if(request.startsWith("KILL_SERVICE")) {
				
						synchronized(this) {server.serverSocket.close(); server.isStopped = true;}
						// System.out.println("Stopped server socket");
						is.close();
						os.close();
						clientSocket.close();

						break;
				}
				else if(request.startsWith("JOIN")) {

					String roomName = requestSplit[1].trim();

					request = is.readLine();		// IP ignore
					// System.out.println(this.toString()+ ": "+request);

					request = is.readLine();		// PORT ignore
					// System.out.println(this.toString()+ ": "+request);

					request = is.readLine();		// user name
					// System.out.println(this.toString()+ ": "+request);

					requestSplit = request.split(":");

					String userName = requestSplit[1].trim();
					InetAddress iadd = this.clientSocket.getLocalAddress();
					String ip = iadd.getHostAddress().toString();
					int port = this.clientSocket.getLocalPort();

					synchronized(this) {

						boolean found = false;
						for(int i = 0; i < rooms.size(); i++) {
							if(rooms.get(i).roomName.equals(roomName)) {
								found = true;
								rooms.get(i).join(userName, os, ip, port, joinIds.size());
								joinIds.add(userName + "@" + i);
								break;
							}
						}
						if(!found) {
							joinIds.ensureCapacity(joinIds.size()+1);
							joinIds.add(userName + "@" + rooms.size());
							rooms.add(new Room(roomName, rooms.size(), userName, os, ip, port, joinIds.size()-1));
						} 
					}
				} // end join room
				else if(request.startsWith("LEAVE")){

					String roomReference = requestSplit[1].trim();

					request = is.readLine();			// joinId
					// System.out.println(this.toString()+ ": "+request);
					requestSplit = request.split(":");
					String joinId = requestSplit[1].trim();


					request = is.readLine();			// username
					// System.out.println(this.toString()+ ": "+request);
					requestSplit = request.split(":");
					String username = requestSplit[1].trim();


					synchronized(this) {
						os.println("LEFT_CHATROOM: " + roomReference + "\nJOIN_ID: " + joinId);
						rooms.get(Integer.parseInt(roomReference)).leave(username);
					}
				}
				else if(request.startsWith("CHAT")) {
					String roomReference = requestSplit[1].trim();

					request = is.readLine();		// join id
					// System.out.println(this.toString()+ ": "+request);

					request = is.readLine();		// client name
					// System.out.println(this.toString()+ ": "+request);
					requestSplit = request.split(":");
					String name = requestSplit[1].trim();

					request = is.readLine();		// message
					// System.out.println(this.toString()+ ": "+request);
					requestSplit = request.split(":");
					String message = requestSplit[1];

					request = is.readLine();		// extra '\n' character in message
					// System.out.println(this.toString()+ ": "+request);


					synchronized(this) {
						rooms.get(Integer.parseInt(roomReference)).chat(name, message);
					}

				}
				else if(request.startsWith("DISCONNECT")){
					request = is.readLine();		//ip
					// System.out.println(this.toString()+ ": "+request);

					request = is.readLine();		//name
					requestSplit = request.split(":");
					String username = requestSplit[1].trim();
					// System.out.println(this.toString()+ ": "+request);

					// method leave in class Room, removes user if he is there. handles leaving message also
					synchronized(this) {
						for(int i = 0; i < rooms.size(); i++) 
							rooms.get(i).leave(username);
					}
							is.close();
							os.close();
							clientSocket.close();
						break;
				}
				else {
					// System.out.println("Error request: "+request);
					os.println("ERROR_CODE: 20\nERROR_DESCRIPTION: Invalid Message.");	
				}

			}// end while forever
			
        } catch (Exception e) {e.printStackTrace();}
    } // end run method
} // end class