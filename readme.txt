SUBMISSION PRINT OUT 30/01/2015

Thank you for your submission.
Test executed by user: lopesf







Testing base protocol.
BEGINNING SOCKET TEST: 2015/01/30 02:08:39pm
Connecting to host: 134.226.32.10 port: 1234
Testing connecting to server.
Socket created.

Attempting to connect to 134.226.32.10:1234 
Connection Successful for host 134.226.32.10 on port 1234.
Sent HELO Message to Server:
HELO BASE_TEST
Recieved Message from server:
HELO BASE_TEST
IP:134.226.32.10
Port:1234
StudentID:11705259
Message has 4 lines (we expect at least 4). 
First String Correct:HELO BASE_TEST 
Second string is correct form IP:XXXX.
IP address is correct: 134.226.32.10
Port number is correct: 1234
Third string reports StudentID as 11705259
baseline message response OK.
Sending unknown message to server. It should handle this.
Tests Successful.
Creating first client connection

Testing connecting to server.
Socket created.

Attempting to connect to 134.226.32.10:1234 
Connection Successful for host 134.226.32.10 on port 1234.
Testing joining server by client1. (Socket Resource id #4)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room1 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room1
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 0 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 0 
Server Join Test Success
Testing join message regarding client1. (Socket Resource id #4)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client1 
Message form is correct: client1 has joined this chatroom. Full message recieved:
 client1 has joined this chatroom.

Server join message recieved successfully.
Testing Leaving

Testing leaving on chatroom by client1. (Socket Resource id #4)
Sending Message to Server:
LEAVE_CHATROOM: 0
JOIN_ID: 0
CLIENT_NAME: client1
 
Send succeeded.
Recieved Message from server:
LEFT_CHATROOM: 0
Room Ref is correct: 0 
JOIN ID is correct: 0 
Leave Test Successful.
Testing for receipt of leaving message regarding client1 (Socket Resource id #4)
Recieved Message from server:
CHAT: 0
Room Ref is correct: 0 
Client name has correct form: client1 
Full message recieved:
 client1 has left this chatroom.


Receipt of message successful.
Test join multiple rooms

Testing joining server by client1. (Socket Resource id #4)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room1 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room1
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 0 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 1 
Server Join Test Success
Testing join message regarding client1. (Socket Resource id #4)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client1 
Message form is correct: client1 has joined this chatroom. Full message recieved:
 client1 has joined this chatroom.

Server join message recieved successfully.
Testing joining server by client1. (Socket Resource id #4)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room2 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room2
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 1 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 2 
Server Join Test Success
Testing join message regarding client1. (Socket Resource id #4)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client1 
Message form is correct: client1 has joined this chatroom. Full message recieved:
 client1 has joined this chatroom.

Server join message recieved successfully.
Test leaving multiple chatrooms

Testing leaving on chatroom by client1. (Socket Resource id #4)
Sending Message to Server:
LEAVE_CHATROOM: 0
JOIN_ID: 2
CLIENT_NAME: client1
 
Send succeeded.
Recieved Message from server:
LEFT_CHATROOM: 0
Room Ref is correct: 0 
JOIN ID is correct: 2 
Leave Test Successful.
Testing for receipt of leaving message regarding client1 (Socket Resource id #4)
Recieved Message from server:
CHAT: 0
Room Ref is correct: 0 
Client name has correct form: client1 
Full message recieved:
 client1 has left this chatroom.


Receipt of message successful.
Testing leaving on chatroom by client1. (Socket Resource id #4)
Sending Message to Server:
LEAVE_CHATROOM: 1
JOIN_ID: 2
CLIENT_NAME: client1
 
Send succeeded.
Recieved Message from server:
LEFT_CHATROOM: 1
Room Ref is correct: 1 
JOIN ID is correct: 2 
Leave Test Successful.
Testing for receipt of leaving message regarding client1 (Socket Resource id #4)
Recieved Message from server:
CHAT: 1
Room Ref is correct: 1 
Client name has correct form: client1 
Full message recieved:
 client1 has left this chatroom.


Receipt of message successful.
Testing multiple clients with single room

Testing joining server by client1. (Socket Resource id #4)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room1 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room1
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 0 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 3 
Server Join Test Success
Testing join message regarding client1. (Socket Resource id #4)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client1 
Message form is correct: client1 has joined this chatroom. Full message recieved:
 client1 has joined this chatroom.

Server join message recieved successfully.
Creating second client connection
Testing connecting to server.
Socket created.

Attempting to connect to 134.226.32.10:1234 
Connection Successful for host 134.226.32.10 on port 1234.
Testing joining server by client2. (Socket Resource id #5)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room1 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room1
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 0 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 4 
Server Join Test Success
Testing join message regarding client2. (Socket Resource id #4)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client2 
Message form is correct: client2 has joined this chatroom. Full message recieved:
 client2 has joined this chatroom.

Server join message recieved successfully.
Testing join message regarding client2. (Socket Resource id #5)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client2 
Message form is correct: client2 has joined this chatroom. Full message recieved:
 client2 has joined this chatroom.

Server join message recieved successfully.
Test multiple clients leaving one room

Testing leaving on chatroom by client2. (Socket Resource id #5)
Sending Message to Server:
LEAVE_CHATROOM: 0
JOIN_ID: 4
CLIENT_NAME: client2
 
Send succeeded.
Recieved Message from server:
LEFT_CHATROOM: 0
Room Ref is correct: 0 
JOIN ID is correct: 4 
Leave Test Successful.
Testing for receipt of leaving message regarding client2 (Socket Resource id #5)
Recieved Message from server:
CHAT: 0
Room Ref is correct: 0 
Client name has correct form: client2 
Full message recieved:
 client2 has left this chatroom.


Receipt of message successful.
Testing for receipt of leaving message regarding client2 (Socket Resource id #4)
Recieved Message from server:
CHAT: 0
Room Ref is correct: 0 
Client name has correct form: client2 
Full message recieved:
 client2 has left this chatroom.


Receipt of message successful.
Testing leaving on chatroom by client1. (Socket Resource id #4)
Sending Message to Server:
LEAVE_CHATROOM: 0
JOIN_ID: 3
CLIENT_NAME: client1
 
Send succeeded.
Recieved Message from server:
LEFT_CHATROOM: 0
Room Ref is correct: 0 
JOIN ID is correct: 3 
Leave Test Successful.
Testing for receipt of leaving message regarding client1 (Socket Resource id #4)
Recieved Message from server:
CHAT: 0
Room Ref is correct: 0 
Client name has correct form: client1 
Full message recieved:
 client1 has left this chatroom.


Receipt of message successful.
Testing multiple clients with multiple rooms

Testing joining server by client1. (Socket Resource id #4)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room1 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room1
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 0 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 5 
Server Join Test Success
Testing join message regarding client1. (Socket Resource id #4)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client1 
Message form is correct: client1 has joined this chatroom. Full message recieved:
 client1 has joined this chatroom.

Server join message recieved successfully.
Testing joining server by client2. (Socket Resource id #5)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room1 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room1
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 0 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 6 
Server Join Test Success
Testing join message regarding client2. (Socket Resource id #4)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client2 
Message form is correct: client2 has joined this chatroom. Full message recieved:
 client2 has joined this chatroom.

Server join message recieved successfully.
Testing join message regarding client2. (Socket Resource id #5)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client2 
Message form is correct: client2 has joined this chatroom. Full message recieved:
 client2 has joined this chatroom.

Server join message recieved successfully.
Creating third client connection
Testing connecting to server.
Socket created.

Attempting to connect to 134.226.32.10:1234 
Connection Successful for host 134.226.32.10 on port 1234.
Testing joining server by client3. (Socket Resource id #6)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room1 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room1
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 0 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 7 
Server Join Test Success
Testing join message regarding client3. (Socket Resource id #4)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client3 
Message form is correct: client3 has joined this chatroom. Full message recieved:
 client3 has joined this chatroom.

Server join message recieved successfully.
Testing join message regarding client3. (Socket Resource id #5)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client3 
Message form is correct: client3 has joined this chatroom. Full message recieved:
 client3 has joined this chatroom.

Server join message recieved successfully.
Testing join message regarding client3. (Socket Resource id #6)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client3 
Message form is correct: client3 has joined this chatroom. Full message recieved:
 client3 has joined this chatroom.

Server join message recieved successfully.
Testing joining server by client1. (Socket Resource id #4)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room2 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room2
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 1 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 8 
Server Join Test Success
Testing join message regarding client1. (Socket Resource id #4)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client1 
Message form is correct: client1 has joined this chatroom. Full message recieved:
 client1 has joined this chatroom.

Server join message recieved successfully.
Testing joining server by client3. (Socket Resource id #6)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room2 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room2
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 1 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 9 
Server Join Test Success
Testing join message regarding client3. (Socket Resource id #4)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client3 
Message form is correct: client3 has joined this chatroom. Full message recieved:
 client3 has joined this chatroom.

Server join message recieved successfully.
Testing join message regarding client3. (Socket Resource id #6)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client3 
Message form is correct: client3 has joined this chatroom. Full message recieved:
 client3 has joined this chatroom.

Server join message recieved successfully.
Creating fourth client connection
Testing connecting to server.
Socket created.

Attempting to connect to 134.226.32.10:1234 
Connection Successful for host 134.226.32.10 on port 1234.
Testing joining server by client4. (Socket Resource id #7)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room2 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room2
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 1 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 10 
Server Join Test Success
Testing join message regarding client4. (Socket Resource id #4)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client4 
Message form is correct: client4 has joined this chatroom. Full message recieved:
 client4 has joined this chatroom.

Server join message recieved successfully.
Testing join message regarding client4. (Socket Resource id #6)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client4 
Message form is correct: client4 has joined this chatroom. Full message recieved:
 client4 has joined this chatroom.

Server join message recieved successfully.
Testing join message regarding client4. (Socket Resource id #7)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client4 
Message form is correct: client4 has joined this chatroom. Full message recieved:
 client4 has joined this chatroom.

Server join message recieved successfully.
Testing Messaging

Testing the sending of a message from client1
Sending Message
CHAT: 0
JOIN_ID: 5
CLIENT_NAME: client1
MESSAGE: hello world from client 1

Send Succeeded.
Testing for receipt of message from client1. (Socket Resource id #4)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
clientname is correct: client1 
Message content is correct:
hello world from client 1

Message receipt successful
Testing for receipt of message from client1. (Socket Resource id #5)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
clientname is correct: client1 
Message content is correct:
hello world from client 1

Message receipt successful
Testing for receipt of message from client1. (Socket Resource id #6)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
clientname is correct: client1 
Message content is correct:
hello world from client 1

Message receipt successful
Testing for no receipt of message from client1. (Socket Resource id #7)
Nothing to read on Resource id #7 socket. This is correct.
Testing the sending of a message from client2
Sending Message
CHAT: 0
JOIN_ID: 8
CLIENT_NAME: client2
MESSAGE: hello world from client 2

Send Succeeded.
Testing for receipt of message from client2. (Socket Resource id #4)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
clientname is correct: client2 
Message content is correct:
hello world from client 2

Message receipt successful
Testing for receipt of message from client2. (Socket Resource id #5)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
clientname is correct: client2 
Message content is correct:
hello world from client 2

Message receipt successful
Testing for receipt of message from client2. (Socket Resource id #6)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
clientname is correct: client2 
Message content is correct:
hello world from client 2

Message receipt successful
Testing for no receipt of message from client2. (Socket Resource id #7)
Nothing to read on Resource id #7 socket. This is correct.
Testing the sending of a message from client3
Sending Message
CHAT: 0
JOIN_ID: 9
CLIENT_NAME: client3
MESSAGE: hello world from client 3

Send Succeeded.
Testing for receipt of message from client3. (Socket Resource id #4)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
clientname is correct: client3 
Message content is correct:
hello world from client 3

Message receipt successful
Testing for receipt of message from client3. (Socket Resource id #5)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
clientname is correct: client3 
Message content is correct:
hello world from client 3

Message receipt successful
Testing for receipt of message from client3. (Socket Resource id #6)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
clientname is correct: client3 
Message content is correct:
hello world from client 3

Message receipt successful
Testing for no receipt of message from client3. (Socket Resource id #7)
Nothing to read on Resource id #7 socket. This is correct.
Testing the sending of a message from client3
Sending Message
CHAT: 1
JOIN_ID: 9
CLIENT_NAME: client3
MESSAGE: hello world from client 3

Send Succeeded.
Testing for receipt of message from client3. (Socket Resource id #4)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
clientname is correct: client3 
Message content is correct:
hello world from client 3

Message receipt successful
Testing for receipt of message from client3. (Socket Resource id #6)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
clientname is correct: client3 
Message content is correct:
hello world from client 3

Message receipt successful
Testing for receipt of message from client3. (Socket Resource id #7)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
clientname is correct: client3 
Message content is correct:
hello world from client 3

Message receipt successful
Testing for no receipt of message from client3. (Socket Resource id #5)
Nothing to read on Resource id #5 socket. This is correct.
Testing Disconnections

Testing sending message to server by client1. (Socket Resource id #4)
Sending Message to Server:
DISCONNECT:0
PORT:0
CLIENT_NAME:client1
Send Succeeded.
Testing for receipt of disconnection message regarding client1. (Socket Resource id #4)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client1 
Full message recieved:
 client1 has left this chatroom.

Message correctly includes name of client that has left.
Message receipt successful.
Testing for receipt of disconnection message regarding client1. (Socket Resource id #5)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client1 
Full message recieved:
 client1 has left this chatroom.

Message correctly includes name of client that has left.
Message receipt successful.
Testing for receipt of disconnection message regarding client1. (Socket Resource id #6)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client1 
Full message recieved:
 client1 has left this chatroom.

Message correctly includes name of client that has left.
Message receipt successful.
Testing for receipt of disconnection message regarding client1. (Socket Resource id #4)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client1 
Full message recieved:
 client1 has left this chatroom.

Message correctly includes name of client that has left.
Message receipt successful.
Testing for receipt of disconnection message regarding client1. (Socket Resource id #6)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client1 
Full message recieved:
 client1 has left this chatroom.

Message correctly includes name of client that has left.
Message receipt successful.
Testing for receipt of disconnection message regarding client1. (Socket Resource id #7)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client1 
Full message recieved:
 client1 has left this chatroom.

Message correctly includes name of client that has left.
Message receipt successful.
Testing the sending of a message from client3
Sending Message
CHAT: 0
JOIN_ID: 9
CLIENT_NAME: client3
MESSAGE: hello world from client 3

Send Succeeded.
Testing for receipt of message from client3. (Socket Resource id #5)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
clientname is correct: client3 
Message content is correct:
hello world from client 3

Message receipt successful
Testing for receipt of message from client3. (Socket Resource id #6)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
clientname is correct: client3 
Message content is correct:
hello world from client 3

Message receipt successful
Testing for no receipt of message from client3. (Socket Resource id #7)
Nothing to read on Resource id #7 socket. This is correct.
Testing for no receipt of message from client3. (Socket Resource id #4)
Nothing to read on Resource id #4 socket as expected.
Testing sending message to server by client2. (Socket Resource id #5)
Sending Message to Server:
DISCONNECT:0
PORT:0
CLIENT_NAME:client2
Send Succeeded.
Testing for receipt of disconnection message regarding client2. (Socket Resource id #5)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client2 
Full message recieved:
 client2 has left this chatroom.

Message correctly includes name of client that has left.
Message receipt successful.
Testing for receipt of disconnection message regarding client2. (Socket Resource id #6)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client2 
Full message recieved:
 client2 has left this chatroom.

Message correctly includes name of client that has left.
Message receipt successful.
Testing sending message to server by client3. (Socket Resource id #6)
Sending Message to Server:
DISCONNECT:0
PORT:0
CLIENT_NAME:client3
Send Succeeded.
Testing for receipt of disconnection message regarding client3. (Socket Resource id #6)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client3 
Full message recieved:
 client3 has left this chatroom.

Message correctly includes name of client that has left.
Message receipt successful.
Testing for receipt of disconnection message regarding client3. (Socket Resource id #6)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client3 
Full message recieved:
 client3 has left this chatroom.

Message correctly includes name of client that has left.
Message receipt successful.
Testing for receipt of disconnection message regarding client3. (Socket Resource id #7)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client3 
Full message recieved:
 client3 has left this chatroom.

Message correctly includes name of client that has left.
Message receipt successful.
Testing the sending of a message from client4
Sending Message
CHAT: 1
JOIN_ID: 10
CLIENT_NAME: client4
MESSAGE: hello world from client 4

Send Succeeded.
Testing for receipt of message from client4. (Socket Resource id #7)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
clientname is correct: client4 
Message content is correct:
hello world from client 4

Message receipt successful
Testing for no receipt of message from client4. (Socket Resource id #4)
Nothing to read on Resource id #4 socket as expected.
Testing for no receipt of message from client4. (Socket Resource id #5)
Nothing to read on Resource id #5 socket as expected.
Testing for no receipt of message from client4. (Socket Resource id #6)
Nothing to read on Resource id #6 socket as expected.
Reconnecting first client
Testing connecting to server.
Socket created.

Attempting to connect to 134.226.32.10:1234 
Connection Successful for host 134.226.32.10 on port 1234.
Testing joining server by client1. (Socket Resource id #8)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room2 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room2
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 1 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 11 
Server Join Test Success
Testing join message regarding client1. (Socket Resource id #7)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client1 
Message form is correct: client1 has joined this chatroom. Full message recieved:
 client1 has joined this chatroom.

Server join message recieved successfully.
Testing join message regarding client1. (Socket Resource id #8)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client1 
Message form is correct: client1 has joined this chatroom. Full message recieved:
 client1 has joined this chatroom.

Server join message recieved successfully.
Testing the sending of a message from client4
Sending Message
CHAT: 1
JOIN_ID: 10
CLIENT_NAME: client4
MESSAGE: hello world from client 4

Send Succeeded.
Testing for receipt of message from client4. (Socket Resource id #8)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
clientname is correct: client4 
Message content is correct:
hello world from client 4

Message receipt successful
Testing for receipt of message from client4. (Socket Resource id #7)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
clientname is correct: client4 
Message content is correct:
hello world from client 4

Message receipt successful
Testing sending message to server by client1. (Socket Resource id #8)
Sending Message to Server:
DISCONNECT:0
PORT:0
CLIENT_NAME:client1
Send Succeeded.
Testing for receipt of disconnection message regarding client1. (Socket Resource id #8)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client1 
Full message recieved:
 client1 has left this chatroom.

Message correctly includes name of client that has left.
Message receipt successful.
Testing for receipt of disconnection message regarding client1. (Socket Resource id #7)
Recieved Message from server: CHAT: 1 
Room Ref is correct: 1 
Clientname is correct: client1 
Full message recieved:
 client1 has left this chatroom.

Message correctly includes name of client that has left.
Message receipt successful.
Testing sending message to server by client4. (Socket Resource id #7)
Sending Message to Server:
DISCONNECT:0
PORT:0
CLIENT_NAME:client4
Send Succeeded.
Reconnecting first client
Testing connecting to server.
Socket created.

Attempting to connect to 134.226.32.10:1234 
Connection Successful for host 134.226.32.10 on port 1234.
Testing joining server by client1. (Socket Resource id #9)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room1 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room1
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 0 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 12 
Server Join Test Success
Testing join message regarding client1. (Socket Resource id #9)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client1 
Message form is correct: client1 has joined this chatroom. Full message recieved:
 client1 has joined this chatroom.

Server join message recieved successfully.
Reconnecting second client
Testing connecting to server.
Socket created.

Attempting to connect to 134.226.32.10:1234 
Connection Successful for host 134.226.32.10 on port 1234.
Testing joining server by client2. (Socket Resource id #10)
Send Succeeded. Testing response.
Recieved Message from server: JOINED_CHATROOM: room1 
First string is correct form JOINED_CHATROOM:XXXX.
Chatroom name is correct: room1
Second string is correct form SERVER_IP:XXXX.
IP string is valid form: 134.226.32.10 
Third string is correct form PORT:XXXX.
Port string is valid form: 1234 
Third string is correct form ROOM_REF:XXXX.
room_ref string is valid form: 0 
Third string is correct form JOIN_ID:XXXX.
join_id string is valid form: 13 
Server Join Test Success
Testing join message regarding client2. (Socket Resource id #10)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client2 
Message form is correct: client2 has joined this chatroom. Full message recieved:
 client2 has joined this chatroom.

Server join message recieved successfully.
Testing join message regarding client2. (Socket Resource id #9)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
Clientname is correct: client2 
Message form is correct: client2 has joined this chatroom. Full message recieved:
 client2 has joined this chatroom.

Server join message recieved successfully.
Testing the sending of a message from client1
Sending Message
CHAT: 0
JOIN_ID: 12
CLIENT_NAME: client1
MESSAGE: hello world from client 1

Send Succeeded.
Testing for receipt of message from client1. (Socket Resource id #9)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
clientname is correct: client1 
Message content is correct:
hello world from client 1

Message receipt successful
Testing for receipt of message from client1. (Socket Resource id #10)
Recieved Message from server: CHAT: 0 
Room Ref is correct: 0 
clientname is correct: client1 
Message content is correct:
hello world from client 1

Message receipt successful
Sending Message to server:
 KILL_SERVICE
 
Succeeded.
Your server should have shutdown...we will now test.
ERROR: Host not disconnected as expected
Tests Complete

All tests passed

Grade: 92 = round( (75 / 75) * 20 + (1755 / 1755) * 70 + (5 / 25) * 10)

No grade possible,