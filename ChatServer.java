import java.net.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatServer implements Runnable{

    private int          serverPort;
    public ServerSocket serverSocket = null;
    public boolean      isStopped    = false;
    private Thread       runningThread= null;
    private ExecutorService threadPool = Executors.newFixedThreadPool(50);

    private static final ArrayList<Room> rooms = new ArrayList<Room>();
    private static final ArrayList<String> joinIds = new ArrayList<String>();


    public ChatServer(int port){ this.serverPort = port; }

    private synchronized boolean isStopped() { return this.isStopped; }

    public void run(){
    	
        synchronized(this){ this.runningThread = Thread.currentThread(); }
        
        openServerSocket();
        while(! isStopped()){
            Socket clientSocket = null;
            try {
                clientSocket = this.serverSocket.accept();
		        //System.out.println("received connection");
            } catch (IOException e) {
                if(isStopped()) {
                    System.out.println("Server Stopped!!") ;
        	        System.exit(0);
                }
                throw new RuntimeException(
                    "Error accepting client connection", e);
            }
            this.threadPool.execute(
                new clientThread(clientSocket, this, rooms, joinIds, threadPool));
        }
    }

    public synchronized void stop(){
        this.isStopped = true;
        try {
            this.serverSocket.close();
		    this.threadPool.shutdown();
		    this.threadPool.shutdownNow();
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }

    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port "+ this.serverPort, e);
        }
    }
	public static void main(String[] args) {
//		ChatServer server = new ChatServer(Integer.parseInt(args[0]));
		ChatServer server = new ChatServer(1234);
		new Thread(server).start();
		
	}
}
