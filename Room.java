import java.io.PrintStream;
import java.util.ArrayList;


public class Room {

	public 	String roomName;
	public 	int roomReference;
	private ArrayList<String> usernames;
	private ArrayList<PrintStream> userOutputStreams;

	// index of a username in the chat room maps to same index of output stream
	
	// constructor called when user joins new chat room
	public Room(String name, int reference, String username, PrintStream userOutputStream,
			String ip, int port, int joinId) {

		this.roomName = name;
		this.roomReference = reference;

		this.usernames = new ArrayList<String>();
		this.usernames.add(username);

		this.userOutputStreams = new ArrayList<PrintStream>();
		this.userOutputStreams.add(userOutputStream);

		userOutputStream.println("JOINED_CHATROOM: "+ roomName +
				"\nSERVER_IP: "+ ip +
				"\nPORT: "+ port +
				"\nROOM_REF: "+ reference +
				"\nJOIN_ID: "+ joinId);

		chat(username, username + " has joined this chatroom.");
	}
	
	// chat method send chat message to all users (output streams) in chat room
	public void chat(String user, String message) {
		for(int i = 0; i < this.userOutputStreams.size(); i++) 
			this.userOutputStreams.get(i).println("CHAT: " + roomReference +
					"\nCLIENT_NAME: " + user + "\nMESSAGE: " + message + "\n");
	}
	
	// called when user joins existing chat room 
	public void join(String user, PrintStream os, String ip, int port, int id) {
		
		this.usernames.ensureCapacity(this.usernames.size()+1);
		this.usernames.add(user);
		this.userOutputStreams.ensureCapacity(this.userOutputStreams.size()+1);
		this.userOutputStreams.add(os);

		// joining message to this user
		os.println("JOINED_CHATROOM: "+ this.roomName +
			"\nSERVER_IP: "+ ip +
			"\nPORT: "+ port +
			"\nROOM_REF: "+ this.roomReference +
			"\nJOIN_ID: "+ id);

		// chat message
		chat(user, user + " has joined this chatroom.");
	}
	
	// method to leave chat room. If user in this chatroom, send chat message, otherwise nothing.
	// method called also for the disconnect command
	public void leave(String username) {
		int index = usernames.indexOf(username);

		if(index != -1) {
			chat(username, username + " has left this chatroom.");
			usernames.remove(index);
			userOutputStreams.remove(index);
		}
	}
}// end class
